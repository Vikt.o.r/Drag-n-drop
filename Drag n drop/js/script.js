const item = document.querySelector('.item');
const placeholders = document.querySelectorAll('.placeholder');
// Item Suggestion Event
item.addEventListener('dragstart', dragstart);
item.addEventListener('dragend', dragend);
// A loop that runs through the insertion elements (has its own events).
for (const placeholder of placeholders) {
    placeholder.addEventListener('dragover', dragover);
    placeholder.addEventListener('dragenter', dragenter);
    placeholder.addEventListener('dragleave', dragleave);
    placeholder.addEventListener('drop', dragdrop);
}
// The event function started moving the element.
function dragstart(event){
    //console.log('drag start', event.target);
    event.target.classList.add('hold');
    setTimeout(() => event.target.classList.add('hide'), 0);
}

// The function of the end of element movement event.
function dragend(event){
    //console.log('drag end');

    //event.target.className = 'item';
    //             or
    event.target.classList.remove('hold', 'hide');
   // event.target.classList.remove('hide');
}

// Cycle processing functions
function dragover(event) {
    event.preventDefault()
    //console.log('drag over');
}
function dragenter(event) {
    event.target.classList.add('hovered');
  //console.log('drag enter');
}
function dragleave(event) {
    event.target.classList.remove('hovered'); 
    
}
function dragdrop(event) {
    event.target.classList.remove('hovered'); 
    event.target.append(item);
}
